package net.jackcroft.math;
import java.text.DecimalFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Jack
 *
 */

public class EfficientRunner extends Thread{
	
	private static Logger logger = LoggerFactory.getLogger(EfficientRunner.class);
	private int start;
	private int runs;
	private int factor;
	private CompressedPrimeWriter handler;
	
	public EfficientRunner(int s, int r, int f, int primeBlockSize, String indexName)
	{
		handler=new CompressedPrimeWriter(primeBlockSize, indexName);
		start=s;
		runs=r;
		factor=f;
	}
	
	//creates and writes a list of primes based off of starting position, number of runs and multiplying factor
	public void factorStarter(int start, int runs, long factor)
	{
		long startTime=System.nanoTime();
		long[] sqrtPrimes=new long[(int) (Math.sqrt((runs+start)*factor)+1)];	
		boolean[] primeArray=new boolean[(int)factor];
		
		for(int i=2;i<sqrtPrimes.length;i++)		//initializing array
		{
			sqrtPrimes[i]=i;
		}
		
		for(int i=2;i<Math.sqrt(sqrtPrimes.length);i++)		//creating primes and putting them in array
		{
			for(int j=i*i;j<sqrtPrimes.length;j+=i)
			{
				sqrtPrimes[j]=0;
			}	
		}
		
		for(int i=0;i<sqrtPrimes.length;i++)		//shifting primes to start of range
		{
			if(Math.pow(sqrtPrimes[i],2)<start*factor&&sqrtPrimes[i]>0)
			{
				long mod=(start*factor)%sqrtPrimes[i];
				if(mod!=0)
					sqrtPrimes[i]=(start*factor)+(sqrtPrimes[i]-mod);
				else
					sqrtPrimes[i]=start*factor;
			}
		}
		
		logger.debug("Pre-Loop Time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
		
		for(int i=0;i<runs;i++)		//make runs through factor series
		{
			primeArray=new boolean[(int)factor];
			long startLoopCalcTime=System.nanoTime();
			long k;
			long limit=((long)Math.sqrt(factor+factor*(i+start)));
			if(start==0&&i==0)		//get rid of #s 0,1
			{
				primeArray[0]=true;
				primeArray[1]=true;
			}
			
			for(int j=0;j<=limit&&j<sqrtPrimes.length;j++)		//loop through prime numbers and cross off multiples
			{
				if(sqrtPrimes[j]!=0)
				{
					if(sqrtPrimes[j]>limit)		//if starting i>100 and not square of prime
					{
						for(k=sqrtPrimes[j];k<(factor+factor*(i+start));k+=j)
						{
							primeArray[(int)k-(int)factor*(i+start)]=true;
						}
						sqrtPrimes[j]=k;
					}
					else		//starting at square of prime
					{
						for(k=(long)Math.pow(j, 2);k<(factor+factor*(i+start));k+=j)
						{
							primeArray[(int)(k-factor*(i+start))]=true;
						}
						sqrtPrimes[j]=k;
					}
				}
			}
			DecimalFormat notation=new DecimalFormat("0E00");
			logger.debug("Loop "+(i+1)+" Calc Time: "+(System.nanoTime()-startLoopCalcTime)/1000000000.+" seconds");
			handler.writeToText(primeArray,"prime"+notation.format(factor*(i+start)),(i+start)*factor,i+start);		//write text of primes of factor series
			logger.info("Total Loop time: "+(System.nanoTime()-startLoopCalcTime)/1000000000.+" seconds"); 
		}
		logger.info("Total Run Time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
		
	}
	
	public void run()
	{
		factorStarter(start, runs, factor);
	}
	
	public static void main(String[] args) {
		String indexName="tempIndex";
		try
		{
			long startTime=System.nanoTime();
			EfficientRunner er1=new EfficientRunner(0, 1, 1000, 20, indexName);
//			EfficientRunner er2=new EfficientRunner(1, 1, 1000000000, 10000000, indexName);
//			EfficientRunner er3=new EfficientRunner(2, 1, 1000000000, 10000000, indexName);
			CompressedPrimeWriter fh=new CompressedPrimeWriter(20, indexName);
			er1.start();
			
//			er2.start();
//			er3.start();
			er1.join();
//			er2.join();
//			er3.join();
			logger.info("Overall Time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
//			System.out.println(fh.readPrimeFromText(456));
		}
		catch(InterruptedException e)
		{
			logger.error(e.getMessage());
		}
	}
}