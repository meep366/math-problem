package net.jackcroft.math;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Jack
 *
 */
public class Compressor {

	private static Logger logger = LoggerFactory.getLogger(Compressor.class);
	private int splitSize;
	private String indexName;
	
	public Compressor(int splitPrimes, String name)
	{
		splitSize=splitPrimes*6;
		indexName=name;
	}
	
	//takes a byte[] and converts it to an array of longs
	public long[] separate(byte[] input)
	{
		long[] primes=new long[input.length/6];
		
		for(int i=0;i<input.length;i+=6)
		{
			primes[i/6]=new BigInteger(Arrays.copyOfRange(input,i,i+6)).longValue();
		}
		
		return primes;
	}
	
	//decompresses a given byte[]
	public long[] decompressPrimes(byte[] compressedBytes)
	{
		long startTime=System.nanoTime();
		long[] list=null;
	    try
	    {
			Inflater decompresser = new Inflater();
			decompresser.setInput(compressedBytes);
			
			byte[] buffer=new byte[splitSize*2];
			ByteArrayOutputStream outputStream=new ByteArrayOutputStream(compressedBytes.length);
			while(!decompresser.finished())
			{
				int count=decompresser.inflate(buffer);
				outputStream.write(buffer, 0, count);
			}
			
			decompresser.end();
			byte[] output=outputStream.toByteArray();
			list=separate(output);
		} 
	    catch (DataFormatException e) 
	    {
			e.printStackTrace();
		}
	    logger.debug("Decompress time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
	    return list;
	}
	
	//gets the number of primes in a block of compressed bytes
	public int getDecompressedNumPrimes(byte[] compressedBytes)
	{
		long startTime=System.nanoTime();
		int length=0;
	    try
	    {
			Inflater decompresser = new Inflater();
			decompresser.setInput(compressedBytes);
			
			byte[] buffer=new byte[splitSize*2];
			ByteArrayOutputStream outputStream=new ByteArrayOutputStream(compressedBytes.length);
			while(!decompresser.finished())
			{
				int count=decompresser.inflate(buffer);
				outputStream.write(buffer, 0, count);
			}
			
			decompresser.end();
			byte[] output=outputStream.toByteArray();
			length=output.length/6;
		} 
	    catch (DataFormatException e) 
	    {
			e.printStackTrace();
		}
	    logger.debug("Decompress time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
	    return length;
	}
	
	//compresses a block of bytes
	public byte[] compressPrimes(byte[] input, String filename, int indexLocation)
	{
		long startTime=System.nanoTime();
		CompressedPrimeWriter fh=new CompressedPrimeWriter(splitSize, indexName);
		byte[] compressedBytes;
		byte[] prime=new byte[6];
		long position=0;
		ArrayList<byte[]> primes=new ArrayList<byte[]>();
		ArrayList<Long> offsets=new ArrayList<Long>();
		Deflater compresser = new Deflater(Deflater.BEST_SPEED);
		ByteArrayOutputStream outputStream=new ByteArrayOutputStream(input.length);
		int totalBytes=0;
		for(int i=0;i*splitSize<input.length;i++)
		{
			int arraySize;
			if(splitSize>input.length)
				arraySize=input.length;
			else if(input.length<splitSize*(i+1))
				arraySize=input.length-splitSize*i;
			else
				arraySize=splitSize;
			
			byte[] temp=new byte[arraySize];
			
			System.arraycopy(input, totalBytes, temp, 0, arraySize);
			totalBytes+=arraySize;
			
			compresser=new Deflater(Deflater.BEST_SPEED);
			compresser.setInput(temp);
			compresser.finish();
			
			primes.add(Arrays.copyOfRange(temp, 0, 6));
			offsets.add(position);
			
			byte[] buffer=new byte[splitSize*2];
			
			while(!compresser.finished())
			{
				int count=compresser.deflate(buffer);
				outputStream.write(buffer, 0, count);
				position+=count;
			}
		}
		compresser.end();
		compressedBytes=outputStream.toByteArray();
		logger.debug("Compress time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
		fh.addToIndex(filename, primes, offsets, input.length/6, indexLocation);
		
		return compressedBytes;
	}
}