package net.jackcroft.math;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 */

/**
 * @author jack
 *
 */
public class Analyzer {

	/**
	 * @param args
	 */
	
	public Analyzer()
	{
		rewrite();
	}
	
	public void rewrite()
	{
		List<Long> primes=writeToList("primeList");
	}
	
	public List<Long> writeToList(String title)
	{
		List<Long> primeStrings=new ArrayList<Long>();
		String temp="";
		File primeListFile=new File(title);
		try 
		{
			FileInputStream primeInput=new FileInputStream(primeListFile);
			while(primeInput.available()>0)
			{
				byte[] b ={new Integer(primeInput.read()).byteValue()};
				
				if(b[0]==10)
				{
					primeStrings.add(Long.valueOf(temp));
					temp="";
				}
				else
					temp+=new String(b);
			}
			primeInput.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return primeStrings;
	}
	
	
	public static void main(String[] args) {

		Analyzer a=new Analyzer();
	}
}