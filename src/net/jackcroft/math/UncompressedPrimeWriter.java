package net.jackcroft.math;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Jack
 *
 */
public class UncompressedPrimeWriter {

	private static Logger logger = LoggerFactory.getLogger(UncompressedPrimeWriter.class);
	
	//returns a list of all the files in the directory
	public File[] listTextFiles()
	{
		String directory="C:\\Users\\Jack\\Desktop\\Programming\\Java Workspace\\Math Problem\\primes";
		File dir = new File(directory);

		return dir.listFiles(new FilenameFilter() 
		{ 
			public boolean accept(File dir, String filename)
			{ return filename.endsWith(""); }
		});
	}

	//finds a specific file in the directory
	public File findFile(String filename)
	{
		File[] files=listTextFiles();
		File current=null;
		for(int i=0;i<files.length;i++)
		{
			if(files[i].getName().equals(filename))
				current=files[i];
		}
		return current;
	}


	//writes the given boolean[] and offset to a file of strings
	public void writeToTextStrings(boolean[] primeArray, String filename, long offset)
	{
		long startTime=System.nanoTime();
		try
		{
			File primeListFile=new File(filename);
			FileOutputStream primeOutput=new FileOutputStream("primes\\"+primeListFile);

			for(int i=0;i<primeArray.length;i++)
			{
				if(!primeArray[i])
				{
					String primeString=BigInteger.valueOf((i+offset)).toString()+'\n';
					primeOutput.write(primeString.getBytes());
				}
			}
			primeOutput.close();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		logger.debug("Write Time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
	}

	//writes the given boolean[] and offset to a file of bytes
	public void writeToTextUncompressed(boolean[] primeArray, String name, long offset) 
	{
		long startTime=System.nanoTime();
		int numPrimes=0;
		for(int i=0;i<primeArray.length;i++)
		{
			if(!primeArray[i])
				numPrimes++;
		}

		File old=findFile(name);
		if(old!=null)
			old.delete();

		RandomAccessFile out;
		try 
		{
			out=new RandomAccessFile("primes\\"+name,"rw");
			FileChannel file = out.getChannel();
			ByteBuffer buf = file.map(FileChannel.MapMode.READ_WRITE, 0, 6 * numPrimes);
			for (int i=0;i<primeArray.length;i++) 
			{
				if(!primeArray[i])
				{
					byte[] bytes=new byte[6];
					byte[] longBytes=BigInteger.valueOf(i+offset).toByteArray();

					for(int j=0;j<longBytes.length;j++)
						bytes[6-longBytes.length+j]=longBytes[j];

					buf.put(bytes);
				}
			}
			file.close();
			out.close();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		logger.debug("Write time: "+(System.nanoTime()-startTime)/1000000000.);
	}	


	//reads in an entire file and returns it as an ArrayList<BigInteger>
	public ArrayList<BigInteger> readFromUncompressedText(String filename)
	{
		long startTime=System.nanoTime();
		ArrayList<BigInteger> primes=new ArrayList<BigInteger>();
		BigInteger bigInt=null;
		try
		{
			boolean done=false;
			byte[] input=new byte[6];
			File primeListFile=new File(filename);
			FileInputStream primeInput=new FileInputStream("primes\\"+primeListFile);
			primeInput.read(input, 0, input.length);
			bigInt=new BigInteger(input);
			primes.add(bigInt);

			while(!done) 
			{
				primeInput.read(input, 0, input.length);
				if(bigInt.equals(new BigInteger(input)))
					done=true;
				else
				{
					bigInt=new BigInteger(input);
					primes.add(bigInt);
				}
			}

			primeInput.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		logger.debug("Read Time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
		return primes;
	}

	//finds a specific prime in the given position, 0 indexed
	public BigInteger readNumberFromText(String filename, long position)
	{
		long startTime=System.nanoTime();
		BigInteger bigInt=null;
		try
		{
			byte[] input=new byte[6];
			File primeListFile=new File(filename);
			FileInputStream primeInput=new FileInputStream("primes\\"+primeListFile);

			primeInput.skip(position*6);

			primeInput.read(input, 0, input.length);
			bigInt=new BigInteger(input);

			primeInput.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		logger.debug("Read Number Time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
		return bigInt;
	}
}