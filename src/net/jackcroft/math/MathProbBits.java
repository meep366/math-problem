package net.jackcroft.math;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 */

/**
 * @author jack
 *
 */
public class MathProbBits {
	
	public static  long NUM=1000;
	public static boolean[] primeArray=new boolean[(int)NUM];
	public static List<Integer> primeList;
	
	public MathProbBits()
	{
		createPrimes();
		writeToText("primeList",0);
	//	factorPrimes(500);
	//	rangePrimes(0,1,"prime");
	//	factorStarter(0,1);
	}
	 
	public void factorStarter(int start, int runs)
	{
		int factor=1000;
		long[] sqrtPrimes=new long[(int) Math.sqrt((runs+start)*factor)];	
		long[] sqrtConstant=new long[sqrtPrimes.length];
		boolean[] primeTempArray=new boolean[(int) factor];
		
		for(int i=2;i<sqrtPrimes.length;i++)		//initializing array
		{
			sqrtPrimes[i]=i;
		}
		
		for(int i=2;i<Math.sqrt(sqrtPrimes.length);i++)		//creating primes and putting them in array
		{
			for(int j=i*i;j<sqrtPrimes.length;j+=i)
			{
				sqrtPrimes[j]=0;
			}	
		}
		
		for(int i=0;i<sqrtPrimes.length;i++)		//making constant array of primes 
		{
			sqrtConstant[i]=sqrtPrimes[i];
				
		}
		
		long startTime=System.nanoTime();
		
		for(int i=0;i<sqrtPrimes.length;i++)		//shifting primes to start of range
		{
			if(Math.pow(sqrtPrimes[i],2)<start*factor&&sqrtPrimes[i]>0)
			{
				long mod=(start*factor)%sqrtPrimes[i];
				if(mod!=0)
					sqrtPrimes[i]=(start*factor)+(sqrtPrimes[i]-mod);
				else
					sqrtPrimes[i]=start*factor;
			}
		}
		
		for(int i=0;i<runs;i++)		//make runs through factor series
		{
			long startLoopCalcTime=System.nanoTime();
			
			for(int j=0;j<factor;j++)
			{
				primeTempArray[j]=true;
			}
			
			long k;
			long limit=((long)Math.sqrt(factor+factor*(i+start)));
			
			if(start==0)		//get rid of #s 0,1
			{
				primeTempArray[0]=false;
				primeTempArray[1]=false;
			}
			
			for(int j=0;j<=limit&&j<sqrtPrimes.length;j++)		//loop through prime numbers and cross off multiples
			{
				if(sqrtPrimes[j]!=0)
				{
					if(sqrtPrimes[j]>limit)		//if starting i>100 and not square of prime
					{
						for(k=sqrtPrimes[j];k<factor+factor*(i+start);k+=sqrtConstant[j])
						{
							primeTempArray[(int)k-(int)factor*(i+start)]=false;
						}
						sqrtPrimes[j]=k;
					}
					else		//starting at square of prime
					{
						for(k= (long)Math.pow(sqrtConstant[(int)j], 2);k<factor+factor*(i+start);k+=sqrtConstant[(int)j])
						{
							primeTempArray[(int)(k-factor*(i+start))]=false;
						}
						sqrtPrimes[j]=k;
					}
				}
			}
			
			System.out.println("Loop "+(i+1)+" Calc Time: "+(System.nanoTime()-startLoopCalcTime)/1000000000.+" seconds");
			primeArray=primeTempArray;
			writeToText("prime"+(i+start),((i+start)*factor));		//write text of primes of factor series
			
		}
		System.out.println("Final Time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
	}
	
	public void rangePrimes(int lower, int upper, String title)
	{
		long range=100;
		long limit= (long)Math.sqrt(range*upper);
		long k;
		long[] sqrtPrimes=new long[(int)Math.sqrt(range*(upper))];
		long[] sqrtConstant=new long[sqrtPrimes.length];
		boolean[] primeTempArray=new boolean[(int) range*(upper-lower)];
		
		for(int i=2;i<sqrtPrimes.length;i++)		//initializing array
		{
			sqrtPrimes[i]=i;
		}
		
		for(int i=2;i<Math.sqrt(sqrtPrimes.length);i++)		//creating primes and putting them in array
		{
			for(int j=i*i;j<sqrtPrimes.length;j+=i)
			{
				sqrtPrimes[j]=0;
			}	
		}
		
		for(int i=0;i<sqrtPrimes.length;i++)		//making constant array of primes 
		{
			sqrtConstant[i]=sqrtPrimes[i];
		}
		
		long startTime=System.nanoTime();
		
		
		for(int i=0;i<sqrtPrimes.length;i++)		//shifting primes to start of range
		{
			if(Math.pow(sqrtPrimes[i],2)<lower*range&&sqrtPrimes[i]>0)
			{
				long mod=(lower*range)%sqrtPrimes[i];
				if(mod!=0)
					sqrtPrimes[i]=(lower*range)+(sqrtPrimes[i]-mod);
				else
					sqrtPrimes[i]=lower*range;
			}
		}
		
		for(int j=0;j<range*(upper-lower);j++)		//Initializing prime array
		{
			primeTempArray[j]=true;
		}
		
		if(lower==0)		//get rid of #s 0,1
		{
			primeTempArray[0]=false;
			primeTempArray[1]=false;
		}
		
			for(long j=0;j<=limit&&j<sqrtPrimes.length;j++)		//loop through prime numbers and cross off multiples
			{
				if(sqrtPrimes[(int)j]!=0)
				{
					if(sqrtPrimes[(int)j]>limit)		//if starting i>100 and not square of prime
					{
						for(k=sqrtPrimes[(int)j];k<range*(upper);k+=sqrtConstant[(int)j])
						{
							primeTempArray[(int)k-(int)range*lower]=false;
						}
						sqrtPrimes[(int)j]=k;
					}
					else		//starting at square of prime
					{
						for(k=(long) Math.pow(sqrtConstant[(int)j], 2);k<range*(upper);k+=sqrtConstant[(int)j])
						{
							primeTempArray[(int)(k-range*lower)]=false;
						}
						sqrtPrimes[(int)j]=k;
					}
				}
			}
			
		System.out.println("Calc Time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
		primeArray=primeTempArray;
		writeToText(title,lower*range);		//write text of primes of factor series
		System.out.println("Final Time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
	}
	
	public void factorPrimes(int start)
	{
		int ntimes=1;			//factors of factor var
		long factor=100;
		
		long[] sqrtPrimes=new long[(int) Math.sqrt(ntimes*factor)];	
		long[] sqrtConstant=new long[sqrtPrimes.length];
		boolean[] primeTempArray=new boolean[(int) factor];
		
		for(int i=2;i<sqrtPrimes.length;i++)		//initializing array
		{
			sqrtPrimes[i]=i;
		}
		
		for(int i=2;i<Math.sqrt(sqrtPrimes.length);i++)		//creating primes and putting them in array
		{
			for(int j=i*i;j<sqrtPrimes.length;j+=i)
			{
				sqrtPrimes[j]=0;
			}	
		}
		
		for(int i=0;i<sqrtPrimes.length;i++)		//making constant array of primes 
		{
			sqrtConstant[i]=sqrtPrimes[i];
				
		}
		
		long startTime=System.nanoTime();
		
		for(int i=0;i<ntimes;i++)		//loop through series of factor var
		{
			long startLoopCalcTime=System.nanoTime();
			
			for(int j=0;j<factor;j++)
			{
				primeTempArray[j]=true;
			}
			
			long k;
			long limit=((long)Math.sqrt(factor+factor*i));
			
			if(i==0)		//get rid of #s 0,1
			{
				primeTempArray[0]=false;
				primeTempArray[1]=false;
			}
			
			
			for(long j=0;j<=limit&&j<sqrtPrimes.length;j++)		//loop through prime numbers and cross off multiples
			{
				if(sqrtPrimes[(int)j]!=0)
				{
					if(sqrtPrimes[(int)j]>limit)		//if starting i>100 and not square of prime
					{
						for(k=sqrtPrimes[(int)j];k<factor+factor*i;k+=sqrtConstant[(int)j])
						{
							primeTempArray[(int)k-(int)factor*i]=false;
						}
						sqrtPrimes[(int)j]=k;
					}
					else		//starting at square of prime
					{
						for(k=(long) Math.pow(sqrtConstant[(int)j], 2);k<factor+factor*i;k+=sqrtConstant[(int)j])
						{
							primeTempArray[(int)(k-factor*i)]=false;
						}
						sqrtPrimes[(int)j]=k;
					}
				}
			}
		System.out.println("Loop "+(i+1)+" Calc Time: "+(System.nanoTime()-startLoopCalcTime)/1000000000.+" seconds");
		primeArray=primeTempArray;
		writeToText("prime"+i,(i*factor));		//write text of primes of factor series
		
		}
		System.out.println("Final Time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
	}
	
	public void createPrimes()
	{
		long startTime=System.nanoTime();
		for(int i=2;i<NUM;i++)
		{
			primeArray[i]=true;
		}
		
		for(int i=2;i<Math.sqrt(primeArray.length);i++)
		{
			for(int j=i*i;j<primeArray.length;j+=i)
			{
				primeArray[j]=false;
			}
		}
		System.out.println("Calc Time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
	}
	
	public void writeToText(String name, long offset)
	{
		long startTime=System.nanoTime();
		try
		{
			File primeListFile=new File(name);
			FileOutputStream primeOutput=new FileOutputStream(primeListFile);
		
			for(int i=0;i<primeArray.length;i++)
			{
				if(primeArray[i])
				{
					String primeString=BigInteger.valueOf((i+offset)).toString()+'\n';
					primeOutput.write(primeString.getBytes());
				}
			}
			primeOutput.close();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		System.out.println("Write Time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
	}
	
	public static void writeToList()
	{
		List<Integer> primeStrings=new ArrayList<Integer>();
		String temp="";
		File primeListFile=new File("primeList");
		try 
		{
			FileInputStream primeInput=new FileInputStream(primeListFile);
			while(primeInput.available()>0)
			{
				byte[] b ={new Integer(primeInput.read()).byteValue()};
				
				if(b[0]==10)
				{
					primeStrings.add(Integer.valueOf(temp));
					temp="";
				}
				else
					temp+=new String(b);
			}
			primeInput.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		primeList=primeStrings;
	}
	
	public static void main(String[] args) 
	{
		MathProbBits mp=new MathProbBits();
	}
}