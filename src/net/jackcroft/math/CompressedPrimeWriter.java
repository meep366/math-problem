package net.jackcroft.math;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Jack
 *
 */
public class CompressedPrimeWriter {

	private static Logger logger = LoggerFactory.getLogger(CompressedPrimeWriter.class);
	private String indexName;
	private Compressor compressor;
	private String file;
	private BigInteger offset;
	private BigInteger prime;
	private BigInteger blockSize;
	
	public CompressedPrimeWriter(int splitPrimes, String index)
	{
		indexName=index;
		compressor=new Compressor(splitPrimes, index);
	}
	
	//returns a list of all the files in the directory
	private File[] listTextFiles()
	{
		String directory="C:\\Users\\Jack\\Desktop\\Programming\\Java Workspace\\Math Problem\\primes";
		File dir = new File(directory);

		return dir.listFiles(new FilenameFilter() 
		{ 
			public boolean accept(File dir, String filename)
			{ return filename.endsWith(""); }
		});
    }
	
	//finds a specific file in the directory
	private File findFile(String filename)
	{
		File[] files=listTextFiles();
		File current=null;
		for(int i=0;i<files.length;i++)
		{
			if(files[i].getName().equals(filename))
				current=files[i];
		}
		return current;
	}
	
	//compresses and writes a file to text
	public void writeToText(boolean[] primeArray, String filename, long offset, int indexPosition)
	{
		long startTime=System.nanoTime();
		
		File old=findFile(filename);
		if(old!=null)
			old.delete();
		
		int numPrimes=0;
		for(int i=0;i<primeArray.length;i++)
		{
			if(!primeArray[i])
				numPrimes++;
		}
		
		byte[] primes=new byte[numPrimes*6];
		int count=0;
		for(int i=0;i<primeArray.length;i++)
		{
			if(!primeArray[i])
			{
				byte[] prime=BigInteger.valueOf(i+offset).toByteArray();
				for(int j=0;j<prime.length;j++)
				{
					primes[(count*6)+(6-prime.length)+j]=prime[j];
				}
				count++;
			}
		}
		logger.debug("Pre-compress time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
		
		byte[] bytes=compressor.compressPrimes(primes,filename, indexPosition);
		
		long restartTime=System.nanoTime();

		RandomAccessFile out;
		try 
		{
			out=new RandomAccessFile("primes\\"+filename,"rw");
			FileChannel file = out.getChannel();
			ByteBuffer buf = file.map(FileChannel.MapMode.READ_WRITE, 0, bytes.length);
			
			buf.put(bytes);
			file.close();
			out.close();
		} 
		catch (IOException e)
		{
			logger.error(e.getMessage());
		}
		double totalTime=((System.nanoTime()-restartTime))/1000000000.;
		logger.debug("Write time: "+totalTime+" seconds");
	}
	
	//reads in a whole compressed file from text
	public ArrayList<Long> readFromText(int fileNum)
	{
		long startTime=System.nanoTime();
		ArrayList<Long> primes=new ArrayList<Long>();
		try
		{
			File index=findFile(indexName);
			FileInputStream primeInput=new FileInputStream(index);
			int skipBytes=211*fileNum+6*(fileNum+1);	//6 given for num primes 211 for rest of file
			primeInput.skip(skipBytes);
			int usedBlocks=primeInput.read();
			for(int i=0;i<usedBlocks;i++)
			{
				long[] sectionPrimes=readBlockFromText(fileNum,i);
				for(int j=0;j<sectionPrimes.length;j++)
					primes.add(sectionPrimes[j]);
			}
			primeInput.close();
		}
		catch(IOException e)
		{
			logger.error(e.getMessage());
		}
		logger.info("Read time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
		
		return primes;
	}

	//reads in a compressed block of text
	private long[] readBlockFromText(String filename, long skipLength, int blockLength)
	{
		long startTime=System.nanoTime();
		long[] primes=null;
		try
		{
			File primeListFile=new File(filename);
			FileInputStream primeInput=new FileInputStream(primeListFile);
			
			byte[] input=new byte[blockLength];
			primeInput.skip(skipLength);
			primeInput.read(input);
			
			long stopTime=System.nanoTime();
			primes=compressor.decompressPrimes(input);
			long restartTime=System.nanoTime();
			primeInput.close();
			double totalTime=((stopTime-startTime+restartTime-System.nanoTime()))/1000000000.;
			logger.debug("Read time: "+totalTime+" seconds");
		}
		catch(IOException e)
		{
			logger.error(e.getMessage());
		}
		return primes;
	}
	
	//adds a spot in the index file for a specific group of primes
	public void addToIndex(String filename, ArrayList<byte[]> primes, ArrayList<Long> offsets, int numPrimes, int writeLocation)
	{
		long startTime=System.nanoTime();
		RandomAccessFile out;
		byte[] nameBytes=filename.getBytes();
		int bufferLength=217;	//90 for filename, 120 for primes and positions, 6 for the num of primes in the block and 1 for number of used blocks
		
		if(primes.size()>10)
		{
			logger.error("More than 10 sections of file to add to index");
		}
		
		try
		{
			File file=findFile(indexName);
			if(file!=null)
				out=new RandomAccessFile(file,"rw");
			else
				out=new RandomAccessFile("primes\\"+indexName,"rw");
			
			FileChannel fileChannel = out.getChannel();
			ByteBuffer buf = fileChannel.map(FileChannel.MapMode.READ_WRITE, writeLocation*bufferLength, bufferLength);
			
			byte[] primeBytes=BigInteger.valueOf(numPrimes).toByteArray();	//bytes for num of primes in this block
			byte[] shiftBytes=new byte[6];
			for(int j=0;j<primeBytes.length;j++)
				shiftBytes[6-primeBytes.length+j]=primeBytes[j];
			
			buf.put(shiftBytes);
			buf.put((byte)primes.size());
			
			for(int i=0;i<10;i++)		//10 is standard num of blocks per file
			{
				if(i<primes.size())
				{
					byte[] prime=primes.get(i);
					buf.put(prime);
					buf.put(nameBytes);
					long offset=offsets.get(i);
					byte[] longBytes=BigInteger.valueOf(offset).toByteArray();
					byte[] bytes=new byte[6];
					for(int j=0;j<longBytes.length;j++)
						bytes[6-longBytes.length+j]=longBytes[j];
					
					buf.put(bytes);
				}
				else
				{
					byte[] bytes=new byte[21];	//9 for string, 6 for prime and offset
					buf.put(bytes);
				}
			}
			
			fileChannel.close();
			out.close();
			logger.debug("Write to index time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
		} 
		catch (IOException e)
		{
			logger.error(e.getMessage());
		}
	}
	
	//reads in a section from the index file for a given position
	private void readFromIndex(int position)
	{
		long startTime=System.nanoTime();
		BigInteger nextOffset;
		
		try
		{
			File indexFile=findFile(indexName);
			FileInputStream primeInput=new FileInputStream(indexFile);
			
			byte[] input=new byte[42];	//6 for prime, 9 for file, 6 for offset and *2 for two sections
			int skipBytes=7*(position/10+1)+position*21;	//6 for primes in the block, 1 for beginning used blocks, and 21 per section
			primeInput.skip(skipBytes);	
			primeInput.read(input);					
			
			prime=new BigInteger(Arrays.copyOfRange(input, 0, 6));
			file= new String(Arrays.copyOfRange(input, 6, 15));
			offset=new BigInteger(Arrays.copyOfRange(input, 15, 21));
			nextOffset=new BigInteger(Arrays.copyOfRange(input, 36, 42));
			
			if(!nextOffset.equals(BigInteger.valueOf(0)))
				blockSize=nextOffset.subtract(offset);
			else
				blockSize=BigInteger.valueOf(findFile(file).length()-offset.longValue());
			
			primeInput.close();
			logger.debug("Read Index time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
		}
		catch(IOException e)
		{
			logger.error(e.getMessage());
		}
	}
	
	//reads in a specific block of compressed primes
	public long[] readBlockFromText(int fileOffset, int sectionNumber)
	{
		long startTime=System.nanoTime();
		readFromIndex(10*fileOffset+sectionNumber);
		logger.info("Read Block time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
		return readBlockFromText("primes\\"+file,offset.longValue(),(int)blockSize.longValue());
	}
	
	//finds a specific given prime
	public Long readPrimeFromText(long primeNum)
	{
		long startTime=System.nanoTime();
		Long prime=null;
		try
		{
			File indexFile=findFile(indexName);
			FileInputStream primeInput=new FileInputStream(indexFile);
			byte[] input=new byte[6];
			long numPrimes=0;
			long currentPrimes=0;
			int blockNum=0;
			while(numPrimes<primeNum)
			{
				blockNum++;
				primeInput.read(input);
				currentPrimes=new BigInteger(input).longValue();
				numPrimes+=currentPrimes;
				primeInput.skip(211);
			}
			blockNum--;
			numPrimes-=currentPrimes;
			primeInput.close();
			
			int[] values=getCorrectBlock(blockNum, (int)(primeNum-numPrimes));
			long[] primes=readBlockFromText(blockNum,values[0]);
			prime=primes[(int)(primeNum-values[1]-numPrimes)-1];
			
		}
		catch(IOException e)
		{
			logger.error(e.getMessage());
		}
		logger.info("Find Prime time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
		return prime;
	}
	
	//determines the correct block in a file for a prime number
	private int[] getCorrectBlock(int fileNum, int primeNum)
	{
		long startTime=System.nanoTime();
		int[] values=new int[2];
		
		try
		{
			File index=findFile(indexName);
			FileInputStream primeInput=new FileInputStream(index);
			int skipBytes=211*fileNum+6*(fileNum+1);	//6 given for num primes 211 for rest of file
			primeInput.skip(skipBytes);
			int usedBlocks=primeInput.read();
			int primeCount=0;
			int i;
			int current=0;
			for(i=0;i<usedBlocks&&primeCount<primeNum;i++)
			{
				readFromIndex(10*fileNum+i);
				current=getPrimesInBlock("primes\\"+file,offset.longValue(),(int)blockSize.longValue());
				primeCount+=current;;
			}
			i--;
			primeCount-=current;
			values[0]=i;
			values[1]=primeCount;
			primeInput.close();
		}
		catch(IOException e)
		{
			logger.error(e.getMessage());
		}
		logger.info("Read time: "+(System.nanoTime()-startTime)/1000000000.+" seconds");
		
		return values;
	}
	
	//gets the number of primes in a certain block
	private int getPrimesInBlock(String filename, long skipLength, int blockLength)
	{
		long startTime=System.nanoTime();
		int length=0;
		try
		{
			File primeListFile=new File(filename);
			FileInputStream primeInput=new FileInputStream(primeListFile);
			
			byte[] input=new byte[blockLength];
			primeInput.skip(skipLength);
			primeInput.read(input);
			
			long stopTime=System.nanoTime();
			length=compressor.getDecompressedNumPrimes(input);
			long restartTime=System.nanoTime();
			primeInput.close();
			double totalTime=((stopTime-startTime+restartTime-System.nanoTime()))/1000000000.;
			logger.debug("Read time: "+totalTime+" seconds");
		}
		catch(IOException e)
		{
			logger.error(e.getMessage());
		}
		return length;
	}
}